# josm-uml-diagrams

A UML class diagram (written in the PlantUML language) that I hastily drawed when working on JOSM in the beginning of 2018.

More diagrams might follow if I will work on JOSM again in the future.

The contents of this repository are released under the CC0 license, see https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt .

![LayerListDialog, etc.](LayerListDialog.png)
